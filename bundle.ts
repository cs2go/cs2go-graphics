import graphicLayer from './graphic.ts';
import graphic from './graphic.ts';
import renderEngine, { controllers, getControllerFor } from './render-engine.ts';
import { Vector as vector } from 'https://gitlab.com/cs2go/cs2go-core/-/raw/v3.x/math/vector.ts';

export const GraphicLayer = graphicLayer;
export const Graphic = graphic;
export const RenderEngine = renderEngine;
export const Vector = vector;

(<any>window).controllers = controllers;
(<any>window).getControllerFor = getControllerFor;