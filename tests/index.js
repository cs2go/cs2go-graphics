const { app, BrowserWindow } = require('electron');
const cmdr = require('commander');

const { basename } = require('path');

cmdr.option('-t, --test <name>', 'The test page to run');
cmdr.parse(process.argv);

const mainWindowOptions = {
	show: false,

	width: 1024,
	height: 768,

	webPreferences: {
		nodeIntegration: true,
		devTools: true
	}
}

app.on('ready', () => {
	const window = new BrowserWindow(mainWindowOptions);
	window.on('ready-to-show', () => window.show());

	const loadFile = (name = 'index') => window.loadFile(`./${name}.html`);

	window.webContents.on('will-navigate', (event, url) => {
		event.preventDefault();
		let link = basename(url.substring(7), '.html');
		loadFile(link);
	});

	loadFile();
});