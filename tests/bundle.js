// Copyright 2018-2020 the Deno authors. All rights reserved. MIT license.

// This is a specialised implementation of a System module loader.

// @ts-nocheck
/* eslint-disable */
let System, __instantiateAsync, __instantiate;

(() => {
  const r = new Map();

  System = {
    register(id, d, f) {
      r.set(id, { d, f, exp: {} });
    },
  };

  async function dI(mid, src) {
    let id = mid.replace(/\.\w+$/i, "");
    if (id.includes("./")) {
      const [o, ...ia] = id.split("/").reverse(),
        [, ...sa] = src.split("/").reverse(),
        oa = [o];
      let s = 0,
        i;
      while ((i = ia.shift())) {
        if (i === "..") s++;
        else if (i === ".") break;
        else oa.push(i);
      }
      if (s < sa.length) oa.push(...sa.slice(s));
      id = oa.reverse().join("/");
    }
    return r.has(id) ? gExpA(id) : import(mid);
  }

  function gC(id, main) {
    return {
      id,
      import: (m) => dI(m, id),
      meta: { url: id, main },
    };
  }

  function gE(exp) {
    return (id, v) => {
      v = typeof id === "string" ? { [id]: v } : id;
      for (const [id, value] of Object.entries(v)) {
        Object.defineProperty(exp, id, {
          value,
          writable: true,
          enumerable: true,
        });
      }
    };
  }

  function rF(main) {
    for (const [id, m] of r.entries()) {
      const { f, exp } = m;
      const { execute: e, setters: s } = f(gE(exp), gC(id, id === main));
      delete m.f;
      m.e = e;
      m.s = s;
    }
  }

  async function gExpA(id) {
    if (!r.has(id)) return;
    const m = r.get(id);
    if (m.s) {
      const { d, e, s } = m;
      delete m.s;
      delete m.e;
      for (let i = 0; i < s.length; i++) s[i](await gExpA(d[i]));
      const r = e();
      if (r) await r;
    }
    return m.exp;
  }

  function gExp(id) {
    if (!r.has(id)) return;
    const m = r.get(id);
    if (m.s) {
      const { d, e, s } = m;
      delete m.s;
      delete m.e;
      for (let i = 0; i < s.length; i++) s[i](gExp(d[i]));
      e();
    }
    return m.exp;
  }

  __instantiateAsync = async (m) => {
    System = __instantiateAsync = __instantiate = undefined;
    rF(m);
    return gExpA(m);
  };

  __instantiate = (m) => {
    System = __instantiateAsync = __instantiate = undefined;
    rF(m);
    return gExp(m);
  };
})();

System.register(
  "https://gitlab.com/cs2go/cs2go-core/-/raw/v3.x/graph/leaf",
  [],
  function (exports_1, context_1) {
    "use strict";
    var Leaf;
    var __moduleName = context_1 && context_1.id;
    return {
      setters: [],
      execute: function () {
        Leaf = class Leaf {
          constructor() {
            this.parent = undefined;
          }
          get root() {
            return this.parentNode ? this.parentNode.root : this;
          }
          get parentNode() {
            return this.parent;
          }
          get pathToNode() {
            let nodes = [];
            let parent = this;
            do {
              nodes.push(parent);
              if (parent.parent) {
                parent = parent.parent;
              }
            } while (parent.parent);
            return nodes;
          }
          setParent(parent) {
            if (this.parent && this.parent.remove) {
              this.parent.remove(this);
            }
            this.parent = parent;
          }
        };
        exports_1("default", Leaf);
      },
    };
  },
);
System.register(
  "https://gitlab.com/cs2go/cs2go-core/-/raw/v3.x/graph/node",
  ["https://gitlab.com/cs2go/cs2go-core/-/raw/v3.x/graph/leaf"],
  function (exports_2, context_2) {
    "use strict";
    var leaf_ts_1, Node;
    var __moduleName = context_2 && context_2.id;
    return {
      setters: [
        function (leaf_ts_1_1) {
          leaf_ts_1 = leaf_ts_1_1;
        },
      ],
      execute: function () {
        Node = class Node extends leaf_ts_1.default {
          constructor() {
            super(...arguments);
            this.__children = [];
          }
          get children() {
            return this.__children;
          }
          add(child) {
            if (this.pathToNode.indexOf(child) > -1) {
              throw new Error("Child node is an acestor to the parent node.");
            }
            child.setParent(this);
            this.__children.push(child);
            if (this.onChildAdded) {
              this.onChildAdded(child);
            }
          }
          remove(child) {
            if (this.children.indexOf(child) > -1) {
              this.__children.splice(this.children.indexOf(child), 1);
            }
          }
          index(i) {
            return this.__children[i];
          }
        };
        exports_2("default", Node);
      },
    };
  },
);
System.register(
  "https://gitlab.com/cs2go/cs2go-core/-/raw/v3.x/math/angles",
  [],
  function (exports_3, context_3) {
    "use strict";
    var degPICalc, rad2deg, deg2rad;
    var __moduleName = context_3 && context_3.id;
    return {
      setters: [],
      execute: function () {
        degPICalc = 180 / Math.PI;
        exports_3("rad2deg", rad2deg = (radians) => radians * degPICalc);
        exports_3("deg2rad", deg2rad = (degrees) => degrees / degPICalc);
      },
    };
  },
);
System.register(
  "https://gitlab.com/cs2go/cs2go-core/-/raw/v3.x/math/vector",
  ["https://gitlab.com/cs2go/cs2go-core/-/raw/v3.x/math/angles"],
  function (exports_4, context_4) {
    "use strict";
    var angles_ts_1, Vector;
    var __moduleName = context_4 && context_4.id;
    return {
      setters: [
        function (angles_ts_1_1) {
          angles_ts_1 = angles_ts_1_1;
        },
      ],
      execute: function () {
        Vector = class Vector {
          constructor(x = 0, y = 0) {
            this.x = x;
            this.y = y;
          }
          /**
                 * Returns the distance from (0|0) -> (vec.x|vec.y)
                 */
          static magnitude(vec) {
            return Math.sqrt(
              (vec.x * vec.x) +
                (vec.y * vec.y),
            );
          }
          /**
                 * Returns a new vector that is of magnitude 1, but maintains the angle.
                 */
          static normal(vec) {
            const length = Vector.magnitude(vec);
            if (length === 0) {
              return new Vector();
            }
            return new Vector(
              vec.x == 0 ? 0 : vec.x / length,
              vec.y == 0 ? 0 : vec.y / length,
            );
          }
          /**
                 * Returns the angle of the supplied vector (in degrees)
                 */
          static angle(vec) {
            return angles_ts_1.rad2deg(Math.atan2(vec.x, vec.y));
          }
          /**
                 * Returns the distance between two Vectors.
                 */
          static distance(vec1, vec2) {
            if (Vector.magnitude(vec1) > Vector.magnitude(vec2)) {
              return Vector.magnitude(Vector.sub(vec1, vec2));
            } else {
              return Vector.magnitude(Vector.sub(vec2, vec1));
            }
          }
          /**
                 * Returns a Vector which is the sum of the two supplied Vectors.
                 */
          static add(vec1, vec2) {
            return new Vector(vec1.x + vec2.x, vec1.y + vec2.y);
          }
          /**
                 * Returns a Vector which is the sum of the first supplied Vector and the inverse if the second.
                 */
          static sub(vec1, vec2) {
            return Vector.add(vec1, Vector.reverse(vec2));
          }
          /**
                 * Returns a Vector that is scaled by a factor of 'scalar' compared to the supplied Vector.
                 */
          static multiply(vec, scalar) {
            return new Vector(vec.x * scalar, vec.y * scalar);
          }
          /**
                 * Returns a Vector that is the fraction of the the supplied Vector.
                 */
          static divide(vec, scalar) {
            return new Vector(vec.x / scalar, vec.y / scalar);
          }
          /**
                 * Returns a Vector whos magnitude is the size of the limit supplied.
                 */
          static limit(vec, limit) {
            return Vector.magnitude(vec) > limit
              ? Vector.multiply(Vector.normal(vec), limit)
              : Vector.clone(vec);
          }
          /**
                 * Returns a Vector that is rotated by 90deg anticlockwise to the supplied Vector.
                 */
          static left(vec) {
            return new Vector(vec.x * -1, vec.y);
          }
          /**
                 * Returns a Vector that is rotated by 90deg clockwise to the supplied Vector.
                 */
          static right(vec) {
            return new Vector(vec.x, vec.y * -1);
          }
          /**
                 * Returns a Vector that is rotated by 180deg from the supplied Vector.
                 */
          static reverse(vec) {
            return Vector.multiply(vec, -1);
          }
          /**
                 * Returns a new Vector that maintains the same magnitude but is facing the direction provided.
                 */
          static rotateTo(vec, angle) {
            const radians = angles_ts_1.deg2rad(Vector.angle(vec));
            const cos = Math.cos(radians);
            const sin = Math.cos(radians);
            return new Vector(
              (cos * vec.x) + (sin * vec.y),
              (cos * vec.y) - (sin * vec.x),
            );
          }
          /**
                 * Returns a new Vector that maintains the same magnitude but is rotated clockwise by the angle provided.
                 */
          static rotateBy(vec, angle) {
            return Vector.rotateTo(vec, Vector.angle(vec) + angle);
          }
          /**
                 * Returns a duplicate Vector.
                 */
          static clone(vec) {
            return new Vector(vec.x, vec.y);
          }
          /**
                 * Applies the values from the second vector to the first.
                 * @param subject the Vector to modify
                 * @param target the Vector to replicate
                 */
          static apply(subject, target) {
            subject.x = target.x;
            subject.y = target.y;
            return subject;
          }
        };
        exports_4("Vector", Vector);
      },
    };
  },
);
System.register(
  "file:///home/lawrence/projects/@cs2go/cs2go-graphics/graphic-layer",
  [
    "https://gitlab.com/cs2go/cs2go-core/-/raw/v3.x/graph/node",
    "https://gitlab.com/cs2go/cs2go-core/-/raw/v3.x/math/vector",
  ],
  function (exports_5, context_5) {
    "use strict";
    var node_ts_1, vector_ts_1, GraphicLayer;
    var __moduleName = context_5 && context_5.id;
    return {
      setters: [
        function (node_ts_1_1) {
          node_ts_1 = node_ts_1_1;
        },
        function (vector_ts_1_1) {
          vector_ts_1 = vector_ts_1_1;
        },
      ],
      execute: function () {
        GraphicLayer = class GraphicLayer extends node_ts_1.default {
          constructor(offset = new vector_ts_1.Vector()) {
            super();
            this.offset = new vector_ts_1.Vector();
            this._fill = "";
            this._stroke = "";
            this.offset = offset;
          }
          get position() {
            return vector_ts_1.Vector.add(
              this.offset,
              this.parentNode.position,
            );
          }
          get fill() {
            return this._fill;
          }
          set fill(fill) {
            this._fill = fill;
            this.ctx.fillStyle = fill;
          }
          get stroke() {
            return this._stroke;
          }
          set stroke(stroke) {
            this._stroke = stroke;
            this.ctx.strokeStyle = stroke;
          }
          get ctx() {
            if (this._ctx) {
              return this._ctx;
            }
            if (this.parentNode) {
              return this.parentNode.ctx;
            }
            throw new ReferenceError(
              "There is no graphic context in this tree",
            );
          }
          add(graphic) {
            super.add(graphic);
          }
          _render() {
            if (!this.ctx) {
              return;
            }
            if (typeof this.render == "function") {
              this.render();
            }
            this._renderChildren();
          }
          _renderChildren() {
            this.children
              .forEach((child) => child._render());
          }
        };
        exports_5("default", GraphicLayer);
      },
    };
  },
);
System.register(
  "file:///home/lawrence/projects/@cs2go/cs2go-graphics/graphic",
  [
    "file:///home/lawrence/projects/@cs2go/cs2go-graphics/graphic-layer",
    "https://gitlab.com/cs2go/cs2go-core/-/raw/v3.x/math/vector",
  ],
  function (exports_6, context_6) {
    "use strict";
    var graphic_layer_ts_1, vector_ts_2, images, Graphic;
    var __moduleName = context_6 && context_6.id;
    return {
      setters: [
        function (graphic_layer_ts_1_1) {
          graphic_layer_ts_1 = graphic_layer_ts_1_1;
        },
        function (vector_ts_2_1) {
          vector_ts_2 = vector_ts_2_1;
        },
      ],
      execute: function () {
        images = new Map();
        Graphic = class Graphic extends graphic_layer_ts_1.default {
          renderImg(url, x, y, w, h) {
            if (images.has(url)) {
              const image = images.get(url);
              if (image && image.hasLoaded) {
                this.ctx.drawImage(
                  image.data,
                  x + this.position.x,
                  y + this.position.y,
                  w,
                  h,
                );
              }
            } else {
              const image = {
                data: new Image(),
                hasLoaded: false,
              };
              image.data.src = url;
              image.data.onload = () => image.hasLoaded = true;
              images.set(url, image);
            }
          }
          renderPoint(point) {
            this.renderBall(point, .5);
          }
          renderBall(point, radius) {
            point = vector_ts_2.Vector.add(point, this.position);
            this.ctx.beginPath();
            this.ctx.arc(point.x, point.y, radius, 0, 360);
            this.ctx.stroke();
            this.ctx.fill();
          }
          renderLine(p1, p2) {
            p1 = vector_ts_2.Vector.add(p1, this.position);
            p2 = vector_ts_2.Vector.add(p2, this.position);
            this.ctx.beginPath();
            this.ctx.moveTo(p1.x, p1.y);
            this.ctx.lineTo(p2.x, p2.y);
            this.ctx.closePath();
            this.ctx.stroke();
          }
          renderRect(p1, p2, p3, p4) {
            [p1, p2, p3, p4] = [p1, p2, p3, p4].map((p) =>
              vector_ts_2.Vector.add(p, this.position)
            );
            this.ctx.beginPath();
            this.ctx.moveTo(p1.x, p1.y);
            this.ctx.lineTo(p2.x, p2.y);
            this.ctx.lineTo(p3.x, p3.y);
            this.ctx.lineTo(p4.x, p4.y);
            this.ctx.lineTo(p1.x, p1.y);
            this.ctx.stroke();
            this.ctx.fill();
          }
        };
        exports_6("default", Graphic);
      },
    };
  },
);
System.register(
  "file:///home/lawrence/projects/@cs2go/cs2go-graphics/render-engine",
  [
    "file:///home/lawrence/projects/@cs2go/cs2go-graphics/graphic-layer",
    "https://gitlab.com/cs2go/cs2go-core/-/raw/v3.x/math/vector",
  ],
  function (exports_7, context_7) {
    "use strict";
    var graphic_layer_ts_2,
      vector_ts_3,
      controllers,
      getControllerFor,
      RenderEngine;
    var __moduleName = context_7 && context_7.id;
    return {
      setters: [
        function (graphic_layer_ts_2_1) {
          graphic_layer_ts_2 = graphic_layer_ts_2_1;
        },
        function (vector_ts_3_1) {
          vector_ts_3 = vector_ts_3_1;
        },
      ],
      execute: function () {
        exports_7("controllers", controllers = new Map());
        exports_7(
          "getControllerFor",
          getControllerFor = (canvas) => controllers.get(canvas),
        );
        RenderEngine = class RenderEngine extends graphic_layer_ts_2.default {
          constructor(canvas) {
            super();
            this._rendering = false;
            if (canvas) {
              this._canvas = canvas;
            } else {
              this._canvas = document.createElement("canvas");
              document.body.appendChild(this._canvas);
            }
            controllers.set(canvas, this);
            this._ctx = this._canvas.getContext("2d");
            window.addEventListener("resize", () => this.applyScreenSize());
            this.applyScreenSize();
            this._rendering = true;
            Promise.resolve().then(() => this._render());
          }
          _render() {
            if (this._rendering) {
              this.ctx
                .clearRect(0, 0, this.screenSize.x, this.screenSize.y);
              this._renderChildren();
            }
            requestAnimationFrame(() => this._render());
          }
          get canvasElement() {
            return this._canvas;
          }
          get screenSize() {
            return vector_ts_3.Vector.clone(this._screenSize);
          }
          get position() {
            return vector_ts_3.Vector.divide(this.screenSize, 2);
          }
          applyScreenSize() {
            this._screenSize = new vector_ts_3.Vector(
              window.innerWidth,
              window.innerHeight,
            );
            this._canvas?.setAttribute("width", this.screenSize.x + "px");
            this._canvas?.setAttribute("height", this.screenSize.y + "px");
          }
          startRendering() {
            this._rendering = true;
          }
          stopRendering() {
            this._rendering = false;
          }
        };
        exports_7("default", RenderEngine);
      },
    };
  },
);
System.register(
  "file:///home/lawrence/projects/@cs2go/cs2go-graphics/bundle",
  [
    "file:///home/lawrence/projects/@cs2go/cs2go-graphics/graphic",
    "file:///home/lawrence/projects/@cs2go/cs2go-graphics/render-engine",
    "https://gitlab.com/cs2go/cs2go-core/-/raw/v3.x/math/vector",
  ],
  function (exports_8, context_8) {
    "use strict";
    var graphic_ts_1,
      graphic_ts_2,
      render_engine_ts_1,
      vector_ts_4,
      GraphicLayer,
      Graphic,
      RenderEngine,
      Vector;
    var __moduleName = context_8 && context_8.id;
    return {
      setters: [
        function (graphic_ts_1_1) {
          graphic_ts_1 = graphic_ts_1_1;
          graphic_ts_2 = graphic_ts_1_1;
        },
        function (render_engine_ts_1_1) {
          render_engine_ts_1 = render_engine_ts_1_1;
        },
        function (vector_ts_4_1) {
          vector_ts_4 = vector_ts_4_1;
        },
      ],
      execute: function () {
        exports_8("GraphicLayer", GraphicLayer = graphic_ts_1.default);
        exports_8("Graphic", Graphic = graphic_ts_2.default);
        exports_8("RenderEngine", RenderEngine = render_engine_ts_1.default);
        exports_8("Vector", Vector = vector_ts_4.Vector);
        window.controllers = render_engine_ts_1.controllers;
        window.getControllerFor = render_engine_ts_1.getControllerFor;
      },
    };
  },
);

const __exp = __instantiate(
  "file:///home/lawrence/projects/@cs2go/cs2go-graphics/bundle",
);
export const GraphicLayer = __exp["GraphicLayer"];
export const Graphic = __exp["Graphic"];
export const RenderEngine = __exp["RenderEngine"];
export const Vector = __exp["Vector"];
